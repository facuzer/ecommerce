﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetallesUsuario.aspx.cs" Inherits="ECommerce.DetallesUsuario" %>

<!DOCTYPE html>

<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="~/css/Site.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="img/logo.png" type="image/png" sizes="32x32"/>
    <link rel="icon" href="img/logo.png" type="image/png" sizes="32x32"/>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" style="margin-top:10%">
            <div class="col-md-12 text-center">
                <asp:Label ID="Label1" runat="server" Text="Nombre:"></asp:Label>
                <asp:Label ID="nombreLbl" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 text-center">
                <asp:Label ID="Label2" runat="server" Text="Dirección:"></asp:Label>
                <asp:Label ID="direccionLbl" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 text-center">
                <asp:Label ID="Label3" runat="server" Text="Telefono:"></asp:Label>
                <asp:Label ID="telefonoLbl" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 text-center">
                <asp:Label ID="Label4" runat="server" Text="Email:"></asp:Label>
                <asp:Label ID="emailLbl" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-12 text-center">
                <asp:Label ID="Label5" runat="server" Text="Usuario:"></asp:Label>
                <asp:Label ID="usuarioLbl" runat="server" Text=""></asp:Label>
            </div>
            <br />
        <div class="row">
            <div class="col-md-12 text-center">
                <asp:Button ID="volverBtn" runat="server" Text="Volver" Width="200px" />
            </div>
        </div>
     </form>
</body>
</html>
