﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ECommerce
{
    public partial class Carrito : System.Web.UI.Page
    {
        ecommerce_service.ecommerce_serviceSoapClient ws = new ecommerce_service.ecommerce_serviceSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            Art_Carrito.Controls.Clear();
            List<ecommerce_service.VentaDTO> ventas = new List<ecommerce_service.VentaDTO>();
            List<ecommerce_service.VentaDetalleDTO> vds = new List<ecommerce_service.VentaDetalleDTO>();
            ecommerce_service.VentaDTO venta = new ecommerce_service.VentaDTO();
            ecommerce_service.VentaCabeceraDTO vc = new ecommerce_service.VentaCabeceraDTO();
            vc.Id = 2;
            vc.IdCliente = 5;
            vc.IdVendedor = 0;
            vc.Observaciones = "Tiene la pija grande";
            DateTime hoy = DateTime.Now;
            vc.Fecha = hoy;
            venta.VentaCabecera = vc;
            ecommerce_service.VentaDetalleDTO vd = new ecommerce_service.VentaDetalleDTO();
            vd.Cantidad = 10;
            vd.Id = 1;
            vd.IdArticulo = 2;
            vd.IdVentaCabecera = vc.Id;
            vd.PrecioUnitario = 4500;
            vds.Add(vd);
            ventas.Add(venta);
            ecommerce_service.ArticuloDTO articulo = new ecommerce_service.ArticuloDTO();
            articulo.Id = 20;
            articulo.Nombre = "Manzanas";
            articulo.PrecioCompra = 200;
            articulo.PrecioVenta = 250;
            articulo.Stock = 50;
            articulo.Descripcion = "Manzana de los dioses";
            

            var div = new HtmlGenericControl("div");
            div.Attributes.Add("class", "col-md-4");


            var divCard = new HtmlGenericControl("div");
            divCard.Attributes.Add("class", "container-fluid text-left");


            Label lbNombre = new Label();
            lbNombre.Attributes.Add("class", "card-text");
            lbNombre.Text = string.Format("Nombre: {0} <br />", articulo.Nombre);

            
            Label lbDescripcion = new Label();
            lbDescripcion.Attributes.Add("class", "card-text");
            lbDescripcion.Text =string.Format("Descripción: {0} <br/>", articulo.Descripcion);

            Label lbPrecio = new Label();
            lbPrecio.Attributes.Add("class", "card-text");
            lbPrecio.Text = string.Format("Precio: {0} <br/>", vd.PrecioUnitario);

            Label lbStock = new Label();
            lbStock.Attributes.Add("class", "card-text");
            lbStock.Text = string.Format("Stock: {0} <br/>", articulo.Stock);

            divCard.Controls.Add(lbNombre);
            divCard.Controls.Add(lbPrecio);
            divCard.Controls.Add(lbDescripcion);
            divCard.Controls.Add(lbDescripcion);
            div.Controls.Add(divCard);
            Art_Carrito.Controls.Add(div);
        }
    }
}